<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

// Add support for the add_editor_style() function. @Link https://developer.wordpress.org/reference/functions/add_editor_style/
add_theme_support( 'editor-styles' );


// Add support for default posts and comments RSS feed links to head.
add_theme_support( 'automatic-feed-links' ); 


// Add support for let WordPress manage the document title. This theme does not use a hard-coded <title> tag in the document head, WordPress will provide it for us.
add_theme_support( 'title-tag' ); 


// Add support for Post Thumbnails on posts and pages. @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
add_theme_support( 'post-thumbnails' ); 


// Add support for responsive embedded content.
add_theme_support( 'responsive-embeds' );


// Add support for custom line height controls.
add_theme_support( 'custom-line-height' );


// Add support for experimental link color control.
add_theme_support( 'experimental-link-color' );


// Add support for experimental cover block spacing.
add_theme_support( 'custom-spacing' );


// Add support for Wide Alignment Support. @link https://developer.wordpress.org/block-editor/developers/themes/theme-support/#wide-alignment
add_theme_support( 'align-wide' ); 


// Add support for Block Styles. @link https://developer.wordpress.org/block-editor/developers/themes/theme-support/#default-block-styles
add_theme_support( 'wp-block-styles' );


// Switch default core markup for search form, comment form, and comments to output valid HTML5.
add_theme_support(
	'html5',
	array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
		'style',
		'script',
		'navigation-widgets',
	)
);


// Add support for core custom logo. @link https://codex.wordpress.org/Theme_Logo
add_theme_support( 'custom-logo',
	array(
		'height'               => 100,
        'width'                => 300,
		'flex-width'           => true,
		'flex-height'          => false,
		'header-text' => array( 'site-title', 'site-description' ),
		'unlink-homepage-logo' => false, 
	)
);