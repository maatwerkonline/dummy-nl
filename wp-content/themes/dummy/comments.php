<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form. The actual display of comments is
 * handled by a callback to custom_comments_callback().
 *
 * @link https://developer.wordpress.org/themes/template-files-section/partial-and-miscellaneous-template-files/comment-template/
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

function custom_comments_callback( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
			?>
			<li class="post pingback">
				<p><?php _e( 'Pingback:', 'maatwerkonline' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( 'Edit', 'maatwerkonline' ), '<span class="edit-link small">', '</span>' ); ?></p>
			</li>
			<?php
			break;
		default :
			?>
			<li <?php comment_class('media d-flex mb-3 pl-0'); ?> id="comment-<?php comment_ID(); ?>">
				<?php
				$avatar_size = 60;
				if ( '0' != $comment->comment_parent ){
					$avatar_size = 60;
				}
				?>
				<div class="comment-image mr-3">
					<?php echo get_avatar( $comment, $avatar_size ); ?>
				</div>
				<?php
				if ( $post = get_post($post_id) ) {
					if ( $comment->user_id === $post->post_author )
					$badgeauthor = '<span class="author small text-primary align-top ml-1" data-toggle="tooltip" data-placement="top" title="'. __( 'Comment by the author', 'maatwerkonline' ).'">'. __( 'Author', 'maatwerkonline' ).'</span>';
			} /* translators: 1: comment author, 2: author badge, 3: date and time */
				?>
				<div class="media-body">
					<div class="comment-author">
						<?php printf( __( '%1$s  %2$s %3$s', 'maatwerkonline' ),
							sprintf( '<span>%s</span>', get_comment_author_link() ),
							sprintf( $badgeauthor ),
							sprintf( _x( '<time pubdate class="small text-muted ml-2">%s ago</time>', '%s = human-readable time difference', 'maatwerkonline' ), human_time_diff( get_comment_time( 'U' ), current_time( 'timestamp' ) ) )
						); ?>
					</div>
					<div class="comment-text">
						<?php if ( $comment->comment_approved == '0' ) : ?>
							<p class="comment-awaiting-moderation small font-italic text-muted"><?php _e( 'Your comment is awaiting moderation.', 'maatwerkonline' ); ?></p>
						<?php else: ?>
							<?php comment_text(); ?>
						<?php endif; ?>
					</div>
					<div class="comment-reply">
						<?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply', 'maatwerkonline' ), 'depth' => $depth, 'max_depth' => $args['max_depth'], 'before' => '<span class="reply-link small">', 'after' => '</span>' ) ) ); ?>
						<?php edit_comment_link(  __( 'Edit', 'maatwerkonline' ), '<span class="small text-muted ml-1 mr-2">-</span><span class="edit-link small">', '</span>' ); ?>
					</div>
				</div>
			</li>

			<?php
			break;
		endswitch;
} ?>

<?php if ( post_password_required() ) : ?>
	<div class="alert alert-warning" role="alert"><p class="nopassword"><?php _e( 'This post is password protected. Enter the password to view any comments.', 'maatwerkonline' ); ?></p></div>
<?php
		/* Stop the rest of comments.php from being processed,
		* but don't kill the script entirely -- we still have
		* to fully load the template.
		*/
		return;
	endif;
?>

<?php if ( have_comments() ) : ?>
	<h3 id="comments-title" class="mb-4"><?php printf( _n( '%s comment', '%s comments', get_comments_number(), 'maatwerkonline' ), number_format_i18n( get_comments_number() ) );?></h3>

	<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
		<nav id="comment-nav-above">
			<h1 class="assistive-text"><?php _e( 'Comment navigation', 'maatwerkonline' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'maatwerkonline' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'maatwerkonline' ) ); ?></div>
		</nav>
	<?php endif; // check for comment navigation ?>

	<ul class="commentlist list-unstyled">
		<?php
			/* Loop through and list the comments. Tell wp_list_comments()
			* to use custom_comments_callback() to format the comments.
			*/
			wp_list_comments( array( 'callback' => 'custom_comments_callback' ) );
		?>
	</ul>

<?php elseif ( ! comments_open() && ! is_page() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
	<div class="alert alert-warning" role="alert"><?php _e( 'Comments are closed.', 'maatwerkonline' ); ?></div>
<?php endif; ?>

<div id="respond" class="mt-4">
	<h3><?php comment_form_title( __('Leave a Comment', 'maatwerkonline'), __('Leave a Reply to %s', 'maatwerkonline') ); ?></h3>

	<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
		<div class="alert alert-warning" role="alert"><?php printf( __( 'You must be %1$s logged in %2$s to post a comment', 'maatwerkonline' ), '<a href="'.get_option('siteurl').'/wp-login.php?redirect_to='.urlencode(get_permalink()).'">' , '</a>' ); ?></div>
	<?php else : ?>
		<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
			<div id="response" class="form-group hidden_label">
				<label for="comment"><?php _e( 'Comment', 'maatwerkonline' ); ?></label>
				<textarea class="form-control required" name="comment" id="comment" cols="100%" rows="10" tabindex="4" placeholder="<?php _e( 'Comment', 'maatwerkonline' ); ?>"></textarea>
			</div>
			<?php if ( $user_ID ) : ?>
				<a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php" class="text-normal small border-0"><?php echo get_avatar( $user_ID, 30 ); ?> <span class="ml-1"><?php echo $user_identity; ?></span></a> <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="<?php _e('Log out of this account', 'maatwerkonline' ); ?>" class="text-muted small ml-3 border-0"><?php _e( 'Log out', 'maatwerkonline' ); ?></a>
			<?php else : ?>
				<div id="response-author" style="display:none;">
					<div class="form-group hidden_label">
						<label for="author"><?php _e( 'Name', 'maatwerkonline' ); ?></label>
						<input type="text" class="form-control required" name="author" id="author" placeholder="<?php _e( 'Name', 'maatwerkonline' ); ?>" value="<?php echo $comment_author; ?>" size="22" tabindex="1"/>
						<?php if ( $is_invalid_rule ) : ?>
							<small id="authorHelp" class="form-text text-danger" role="alert"><?php _e( 'Please enter a valid email address', 'maatwerkonline' ) ?></small>
						<?php endif; ?>
					</div>
					<div class="form-group hidden_label mb-0">
						<label for="email"><?php _e( 'Email address', 'maatwerkonline' ); ?></label>
						<input type="text" class="form-control required" name="email" id="email" placeholder="<?php _e( 'Email address', 'maatwerkonline' ); ?>" value="<?php echo $comment_author_email; ?>" size="22" tabindex="2"/>
						<small id="emailHelp" class="form-text text-muted"><i class="mo-icon-top mr-2"></i> <?php _e( 'Will not be published.', 'maatwerkonline' ); ?></small>
					</div>
				</div>
			<?php endif; ?>
			<button name="submit" type="submit" id="submit" tabindex="5" class="btn btn-primary float-right"/><?php _e( 'Respond', 'maatwerkonline' ); ?></button>
			<?php cancel_comment_reply_link( '<span class="btn btn-link float-right text-muted">'. __( 'Cancel Reply', 'maatwerkonline').'</span>' );?>
			<?php comment_id_fields(); ?>
			<?php do_action('comment_form', $post->ID); ?>
		</form>
	<?php endif; ?>
</div>
