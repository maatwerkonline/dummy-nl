<?php
/**
 * Functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


// Constants
define('THEME_NAME', 'Dummy');
define('THEME_VERSION', '1.0.0');
define('TEMPPATH', get_template_directory_uri());
define('IMAGES', TEMPPATH . '/assets/images');

function gtm4wp_add_profit_datalayer_data( $dataLayer ){
	$dataLayer["ecomm_profit"] = 'test';

    return $dataLayer;
}

add_filter( 'gtm4wp_compile_datalayer', 'gtm4wp_add_profit_datalayer_data' , 20);

// Check if function theme_by_maatwerkonline_setup() exists
if ( ! function_exists( 'theme_by_maatwerkonline_setup' ) ) {
    
    /** 
     * Sets up theme defaults and registers support for various WordPress features. 
     * Note that this function is hooked into the after_setup_theme hook, which runs before the init hook. 
     * The init hook is too late for some features, such as indicating support for post thumbnails.
     */
    function theme_by_maatwerkonline_setup() {

        /** 
         * Load Theme Textdomain. Make theme available for translation. 
         * 
         * @link https://developer.wordpress.org/reference/functions/load_theme_textdomain/
         */
        load_theme_textdomain( 'maatwerkonline', TEMPPATH . '/languages' );


        /** 
         * Register Nav Menus.
         * 
         * @link https://developer.wordpress.org/themes/functionality/navigation-menus/
         */
        register_nav_menus( array(
            'default' => __('Default Menu', 'maatwerkonline'),
            'topbar' => __('Topbar Menu', 'maatwerkonline'),
            'mobile' => __('Mobile Menu', 'maatwerkonline')
        ) );


        // Add Image Size
        // Would you like to add some Image Sizes? Add the add_image_size( $name, $width, $height, $crop ); right here. Documentation: https://developer.wordpress.org/reference/functions/add_image_size/

        /** 
         * Theme Support.
         * 
         * @link https://developer.wordpress.org/block-editor/developers/themes/theme-support/
         */
        include 'inc/theme-support.php';


        // Register Block Styles
        include 'inc/register-block-styles.php';


        // Load the Block Editor Functionalities
        include 'inc/functions-block-editor.php'; 


        // Functions Menu
        include 'inc/functions-menu.php';


        // Additional Custom Post Types
        // Looking for a place to add Additional Custom Post Types? We use the plugin 'MB Custom Post Types & Custom Taxonomies' for this: /wp-admin/edit.php?post_type=mb-post-type


        // Additional Custom Taxonomies
        // Looking for a place to add Additional Custom Taxonomies? We use the plugin 'MB Custom Post Types & Custom Taxonomies' for this: /wp-admin/edit.php?post_type=mb-post-type


        // Additional Fields
        // Looking for a place to add Additional Fields? We use the plugin 'Advance Custom Fields Pro' for this: /wp-admin/edit.php?post_type=acf-field-group

    }
}
add_action( 'after_setup_theme', 'theme_by_maatwerkonline_setup' );


/**
 * Add Styles.
 * 
 * @link https://developer.wordpress.org/reference/functions/wp_enqueue_scripts/
 * @link https://developer.wordpress.org/reference/functions/wp_enqueue_style/
 */
function theme_styles() {
    // If you would like to use Google Font (https://fonts.google.com/) you can add the font-css here
    wp_enqueue_style('font-css','https://fonts.googleapis.com/css2?family=Open+Sans:wght@300,600&display=swap');

    // Fetch global styles for editor and front-end with theme.json
    wp_enqueue_style( 'slick-slider', TEMPPATH . '/assets/css/slick.css' );
    wp_enqueue_style( 'global-styles-theme', TEMPPATH . '/assets/css/global-styles.css' );
    wp_enqueue_style( 'bootstrap', TEMPPATH . '/assets/css/bootstrap.min.css', array(), THEME_VERSION );
    wp_enqueue_style( 'theme-style', TEMPPATH . '/style.css', array(), THEME_VERSION );
}
add_action('wp_enqueue_scripts', 'theme_styles');


/**
 * Add Scripts.
 * 
 * @link https://developer.wordpress.org/reference/functions/wp_enqueue_scripts/
 * @link https://developer.wordpress.org/reference/functions/wp_enqueue_script/
 */
function theme_scripts() {
    wp_enqueue_script( 'slick-slider', TEMPPATH . '/assets/js/slick-slider.min.js', array('jquery'), '', false );
    wp_enqueue_script( 'init', TEMPPATH . '/assets/js/init.js', true );
    
    // Only Enqueue Comment Reply Script on frontend if is single, if comments are open and if threaded comments are enabled. @link https://peterwilson.cc/including-wordpress-comment-reply-js/
    if ( is_singular() && comments_open() && get_option('thread_comments') ) {
        wp_enqueue_script( 'comment-reply' );
    }
    wp_localize_script('init', 'objectL10n', array(
        'ajaxurl' => admin_url('admin-ajax.php'),
        'post_id' => get_the_ID()
    ));
}
add_action('wp_enqueue_scripts', 'theme_scripts');


?>
