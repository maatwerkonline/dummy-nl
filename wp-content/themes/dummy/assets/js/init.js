jQuery(document).ready(function($) {
	"use strict";

	// Smooth Scroll 
	$('a[href*="#"]').not('[href="#"]').not('[href="#!"]').not('[href="#0"]').not('[data-commentid]').click(function(event) { // Remove links that don't actually link to anything
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) { // On-page links
			// Figure out element to scroll to
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			// Does a scroll target exist?
			if (target.length) {
				// Only prevent default if animation is actually gonna happen
				event.preventDefault();
				$('html, body').animate({
					scrollTop: target.offset().top
				}, 1000, function() {
					// Callback after animation
					// Must change focus!
					var $target = $(target);
					$target.focus();
					if ($target.is(":focus")) { // Checking if the target was focused
						return false;
					} else {
						$target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
						$target.focus(); // Set focus again
					};
				});
			}
		}
	});


	// Mobile menu sidebar
	$('#page-content-wrapper').on('click', function() {
		if($('#wrapper').hasClass('toggled')) {
			$('body').toggleClass('menu-open');
			$('#menu-toggle').removeClass('active');
			$('#wrapper').removeClass('toggled');
		};
	}).on('click', '#menu-toggle', function(e) {
		e.stopPropagation();
		if($('#menu-toggle').hasClass('inactive')) {
			$('body').toggleClass('menu-open');
			parent.history.back();
			return false;
		} else {
			$('body').toggleClass('menu-open');
			$(this).toggleClass('active');
			$('#wrapper').toggleClass('toggled');
		}
	});

	
	// Carousel 
    window.slickSlide = function () {
        $(".carousel").each(function () {
            var $this = $(this);
            var accessibility = ($this.data('accessibility') == 1 ? true : false);
            var adaptiveHeight = ($this.data('adaptiveheight') == 1 ? true : false);
            var autoplay = ($this.data('autoplay') == 1 ? true : false);
            var focusOnSelect = ($this.data('focusonselect') == 1 ? true : false);
            var centerMode = ($this.data('centermode') == 1 ? true : false);
            var arrows = ($this.data('arrows') == 1 ? true : false);
            var dots = ($this.data('dots') == 1 ? true : false);
            var fade = ($this.data('effect') == 'fade' ? true : false);
            var infinite = ($this.data('infinite') == 1 ? true : false);
            var initialSlide = ($this.data('initialslide') - 1);
            var pauseOnHover = ($this.data('pauseonhover') == 1 ? true : false);
            var swipe = ($this.data('swipe') == 1 ? true : false);
            var variableWidth = ($this.data('variablewidth') == 1 ? true : false);
            var responsive = ($this.data('responsive') == 1 ?
                    [{
                            breakpoint: 1200,
                            settings: {
                                slidesToShow: $this.data('slidestoshow_1190'),
                                slidesToScroll: $this.data('slidestoscroll_1190'),
                            }
                        },
                        {
                            breakpoint: 768,
                            settings: {
                                slidesToShow: $this.data('slidestoshow_768'),
                                slidesToScroll: $this.data('slidestoscroll_768'),
                            }
                        },
                        {
                            breakpoint: 576,
                            settings: {
                                slidesToShow: $this.data('slidestoshow_480'),
                                slidesToScroll: $this.data('slidestoscroll_480'),
                            }
                        }
                    ] : false);
            $this.slick({
                accessibility: accessibility,
                adaptiveHeight: adaptiveHeight,
                autoplay: autoplay,
                autoplaySpeed: $this.data('autoplayspeed'),
                arrows: arrows,
                easing: $this.data('easing'),
                centerMode: centerMode,
                dots: dots,
                focusOnSelect: focusOnSelect,
                fade: fade,
                infinite: infinite,
                initialSlide: initialSlide,
                asNavFor: $this.data('asnavfor'),
                pauseOnHover: pauseOnHover,
                speed: $this.data('speed'),
                swipe: swipe,
                variableWidth: variableWidth,
                slidesToShow: $this.data('slidestoshow'),
                slidesToScroll: $this.data('slidestoscroll'),
                rows: $this.data('rows'),
                responsive: responsive,
            });
        });
    }
    window.slickSlide();
});
