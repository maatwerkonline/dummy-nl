// Add Full Width alignment options for all Blocks except for Gravity Forms block, because the Gravity Forms block doesn't support full width
wp.hooks.addFilter(
    'blocks.registerBlockType',
    'maatwerkonline/extra-block-settings',
    function (settings, name) {
        if (name !== 'gravityforms/form') {
            return lodash.assign({}, settings, {
                supports: lodash.assign({}, settings.supports, {
                    align: ['full'],
                }),
            });
        }
        return settings;
    }
);

wp.domReady( () => {
	wp.blocks.unregisterBlockType( 'core/query' );
} );