<?php
$author_id = get_the_author_ID(get_the_id());
$author = get_author_name($author_id);
$categories = wp_get_post_categories(get_the_id());
?>
<div class="col-md-4 col-12 blog_article mb-4">
    <a href="<?php echo get_permalink(); ?>" class="blog_article__content_holder">
        <div class="blog_article__content_holder__image">
            <img src="<?php echo get_the_post_thumbnail_url(get_the_id(), 'medium'); ?>">
            <?php if($categories) : ?>
                <div class="blog_article__content_holder__image__categories">
                    <?php foreach($categories as $category) :
                        $cat = get_category( $category );
                        $cat_id = get_cat_ID( $cat->name );
                        $cat_ids[] = get_cat_ID( $cat->name );
                        ?>
                        <span href="<?php echo get_site_url(); ?>/boostz/blog/?term=<?php echo $cat_id; ?>" class="mr-3"><?php echo $cat->name; ?></span>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
        <h6 class="blog_article__content_holder__title pl-3 pr-3 pt-3">
            <?php
                echo get_the_title();
            ?>
        </h6>
        <div class="blog_article__content_holder__author_date d-flex pl-3 pr-3 mb-3">
            <p class="my-auto"><?php echo $author; ?></p>
            <div class="ml-auto my-auto">
                <p class="mb-0 text-right">
                    <svg id="time" xmlns="http://www.w3.org/2000/svg" width="20.078" height="20.078" viewBox="0 0 20.078 20.078">
                        <g id="Ellipse_14" data-name="Ellipse 14" fill="none" stroke="#000" stroke-width="2">
                            <circle cx="10.039" cy="10.039" r="10.039" stroke="none"/>
                            <circle cx="10.039" cy="10.039" r="9.039" fill="none"/>
                        </g>
                        <path id="Path_23" data-name="Path 23" d="M686.457,1531.446v6.418l3.871,2.343" transform="translate(-676.709 -1527.396)" fill="none" stroke="#000" stroke-linecap="round" stroke-width="2"/>
                    </svg>
                    <span><?php echo get_the_date( 'm-d-Y' ) ?> </span>
                </p>							
            </div>
        </div>
        <div class="blog_article__content_holder__content pl-3 pr-3 pb-2">
            <p>
                <?php
                    $excerpt = get_the_excerpt();
                    echo $excerpt = wp_trim_words( $excerpt, 35, '...');
                ?>
            </p>
        </div> 
        <div class="blog_article__content_holder__read_more pb-4">
            <div class="wp-block-button is-style-fill">
                <span href="<?php echo get_permalink(); ?>" class="wp-block-button__link">
                    <?php _e('Read more','maatwerkonline') ?>
                </span>   
            </div>
        </div> 
    </a>   
</div>  