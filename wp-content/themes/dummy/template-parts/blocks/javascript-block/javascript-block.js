(function($){
	/**
	* initializeBlock
	*
	* Adds custom JavaScript to the block HTML.
	*
	* @param   object $block The block jQuery element.
	* @param   object attributes The block attributes (only available when editing).
	* @return  void
	*/

	// Place custom code here
	var initializeBlock = function() {
	
	}

	// Initialize dynamic block preview (editor).
	if( window.acf ) {
		window.acf.addAction( 'render_block_preview/type=javascript-block', initializeBlock );
	} else {
		$(document).ready(function(){
			initializeBlock();
		});
	}
})(jQuery);
				