<?php
 /**
 * Function for Adding the Import & Export submenu in the Block Tab
 */ 
function acf_pro_for_acf_blocks_submenu_page() {
    // Add submenu for Export Blocks
	add_submenu_page(
		'edit.php?post_type=mo-block',
		'Export Blocks',
		'Export Blocks',
		'manage_options',
		'block-export-submenu-page',
		'acf_blocks_export_submenu_page_callback' // The Callback function for what to show on the Export Blocks tab
	);

    // Add submenu for Import Blocks
	add_submenu_page(
		'edit.php?post_type=mo-block',
		'Import Blocks',
		'Import Blocks',
		'manage_options',
		'block-import-submenu-page',
		'acf_blocks_import_submenu_page_callback' // The Callback function for what to show on the Import Blocks tab
	);
}
add_action('admin_menu', 'acf_pro_for_acf_blocks_submenu_page');

	
/**
 * Function for Exporting the Blocks and make the JSON file
 */ 
function acf_blocks_export_submenu_page_callback() {

    // Check the Export Action
	if(isset($_REQUEST['chk']) && !empty($_REQUEST['chk'])){
		ob_clean();
		$blockID = $_REQUEST['chk'];
		$tempArray = array();
		$csv_header = '';
		$theme_dir = get_theme_file_path('/template-parts/blocks');
		
		foreach($blockID as $keyID){
			$blockDta = get_post($keyID);
			$blockName = $blockDta->post_name;
			$blockDta_json	=  $blockDta->post_content;
			$blockPHP = $theme_dir.'/'.$blockName.'/content-'.$blockName.'.php';
			$blockJS = $theme_dir.'/'.$blockName.'/'.$blockName.'.js';
			$blockCSS = $theme_dir.'/'.$blockName.'/'.$blockName.'.css';

            // Check if the PHP file exists or not
			if(file_exists($blockPHP)){
				$dta = htmlentities(file_get_contents($blockPHP));
			}

            // Check if the CSS file exists or not
			$cssFile = false;
			if(file_exists($blockCSS)){
				$array_css = file($blockCSS);
				$dtacss ='';
				foreach ($array_css as $key => $value) {
					$dtacss .= $value;
				}
				$cssFile = true;
			}

            // Check if the JS file exists or not
			$jsFile = false;
			if(file_exists($blockJS)){
				$array_js = file($blockJS);
				$dtajs ='';
				foreach ($array_js as $key => $value) {
					$dtajs .= $value;
				}
				$jsFile = true;
			}
			
            // Get the Block content and then decode the JSON content
			$blockDta_array = json_decode($blockDta_json);
			$fileArray = array('php' => $dta, 'css' => $dtacss , 'js' => $dtajs, 'cssfile' => $cssFile, 'jsfile' => $jsFile,  );

			foreach($blockDta_array as $key => $value){
				$blcontentarr[$key] = $value;
			}
			
			// Merge the array for creating the final JSON file
			$blockDtas_array = array_merge($blcontentarr, $fileArray);
			array_push($tempArray, $blockDtas_array);
		}
			
		// Export the block.json file
		$mybhzgd = json_encode($tempArray);
		$blockdir = plugin_dir_path( __FILE__ ).'block.json';
		$fp = fopen($blockdir, 'w');
		fwrite($fp, json_encode($tempArray));
		fclose($fp);

		$fileName = 'block.json';
		
        // Adding the header information
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Disposition: attachment; filename=$fileName");
		header("Content-Type: application/zip");
		header("Content-Transfer-Encoding: binary"); 
		readfile($blockdir);
		unlink($blockdir);
		die();
	}
    ?>

    <!-- Design View for Export page -->
	<div class="wrap"><div id="icon-tools" class="icon32"></div>
		
		<?php $args = array(
			'post_type' => 'mo-block', 
			'post_status'    => 'publish'
		);
		$block_post	= get_posts($args);

		if(!empty($block_post)){ ?>
	
			<h1><?php _e('Export Blocks', 'acf-block-generator'); ?></h1>
			<p><?php _e('When you have checked the Blocks you would like to export and then click the button below WordPress will create an JSON file for you to save to your computer.', 'acf-block-generator'); ?></p>
			<p><?php _e('This format will contain your selected blocks.', 'acf-block-generator'); ?></p>
			<p><?php _e('Once you’ve saved the download file, you can use the Import Blocks function in another WordPress installation to import the Blocks from this site.', 'acf-block-generator'); ?></p>
			
            <form action="" method="post">
			
				<table class="wp-list-table widefat fixed striped table-view-list"> 
					<thead>
						<tr>
							<th class="check-column"></th>
							<th class="manage-column column-title column-primary"><?php _e('Block Title', 'acf-block-generator'); ?></th>
							<th><?php _e('Published', 'acf-block-generator'); ?></th>
						</tr>
					</thead>
					<tbody id="the-list">
						<?php foreach ($block_post as $key) { ?>
							<tr class="iedit author-self level-0 type-mo-block status-publish hentry entry">
								<td class="check-column" style="padding: 6px 6px;"><input type="checkbox" value="<?php echo $key->ID; ?>" name="chk[]" /></td>
								<td class="title column-title has-row-actions column-primary page-title"><?php echo $key->post_title; ?></td> 
								<td><?php echo $key->post_date; ?></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>

				<button name="btnexport" class="button button-primary" style="margin: 15px 0px;"><?php _e('Download Export File', 'acf-block-generator'); ?></button>

			</form>
		
		<?php } else { ?>
			<h1><?php _e('Export Blocks', 'acf-block-generator'); ?></h1>
			<p><?php _e('No Blocks found to export.', 'acf-block-generator'); ?></p>
		<?php } ?>

	</div>

    <?php 
}


/**
 * Function for Importing the Blocks and create the Block into Block tab
 */ 
function acf_blocks_import_submenu_page_callback() {
	$status = false; // Adding the variable for the Block status and by default set is as false
	$duplicate = false; // Adding the variable for the Duplicate Block status and by default set is as false
	
    global $wpdb;
	$table_name = $wpdb->prefix.'posts';

    // Getting active theme path
	$output_dir = get_theme_file_path('/template-parts/blocks');

    // Check the Import action
	if(isset($_REQUEST['import'])){
		$blockList = array();
		$data_st =array();
		$data = file_get_contents($_FILES['upload']['tmp_name']);
		$data_decode = json_decode($data);
		
		foreach($data_decode as $key => $value){
			$data_st[$key] = $value;
		}

        // Fetch the export Block data using a loop
		foreach($data_st as $keyd => $valued){
			$php = htmlspecialchars_decode($valued->php);
			$js  = $valued->js;
			$css = $valued->css;
			$cssfile = $valued->cssfile;
			$jsfile = $valued->jsfile;

			$blockName = $valued->title;
			$blockSlug = $valued->name;

			// Check for Block Duplicacy 
			$sql = "SELECT * FROM $table_name WHERE `post_title` = '$blockName' AND `post_type` = 'mo-block' AND `post_status` = 'publish'";
			$chkblock = $wpdb->get_results($sql);
	
			if(empty($chkblock) ){ // Check if Block already exists or not
				
				unset($valued->php); // Remove PHP keywork from the array
				unset($valued->css); // Remove CSS keywork from the array
				unset($valued->js); // Remove JS keywork from the array
				unset($valued->cssfile); // Remove CSS file keywork from the array
				unset($valued->jsfile); // Remove JS file keywork from the array

				$blockJson = json_encode($valued);
				
				// Create the Block based on the JSON file
				$result = $wpdb->insert($table_name, array(
					'post_title' => $blockName,
					'post_name' => $blockSlug,
					'post_type' => 'mo-block',
					'post_content' => $blockJson,
					'post_author' => 1, 
					'post_date' => date('Y-m-d H:i:s'), 
					'post_date_gmt' => date('Y-m-d H:i:s'),
					'post_modified' => date('Y-m-d H:i:s'),
					'post_modified_gmt' => date('Y-m-d H:i:s'),
					'comment_status' => 'closed', 
					'ping_status' => 'closed'
				));

                // Checking Insert Action 
				if($result == true){
					$blockID = $wpdb->insert_id;
					update_post_meta($blockID ,'block_name', $blockSlug);
					$themepath = get_theme_file_path();

			        // Check for the Required Folders
					if(!file_exists($themepath.'/template-parts')){
						@mkdir($themepath . '/template-parts', 0777);
						if(!file_exists($themepath.'/template-parts/blocks/')){
							@mkdir($themepath . '/template-parts/blocks/', 0777);
						}
					}

					if(file_exists($themepath.'/template-parts')){
						if(!file_exists($themepath.'/template-parts/blocks/')){
							@mkdir($themepath . '/template-parts/blocks/', 0777);
						}
					}

					$blockPHP = $output_dir.'/'.$blockSlug.'/content-'.$blockSlug.'.php'; // Creating PHP file dir path
					$blockJS = $output_dir.'/'.$blockSlug.'/'.$blockSlug.'.js'; // Creating JS file dir path
					$blockCSS = $output_dir.'/'.$blockSlug.'/'.$blockSlug.'.css'; // Creating CSS file dir path

			        // Create the PHP, JS and CSS files based on the Block settings
					if (!file_exists($output_dir . $blockSlug)) {
						@mkdir($output_dir .'/'.$blockSlug, 0777);  
						
						if(!file_exists($blockPHP)){	
							$fp = fopen($blockPHP, 'w'); // Open PHP file 
							fwrite($fp, $php);
							fclose($fp);
						}
					
						if(!file_exists($blockCSS) && $cssfile == true ){
							$fpcss = fopen($blockCSS, 'w'); // Open CSS file  
							fwrite($fpcss, $css);
							fclose($fpcss);
						}

						if(!file_exists($blockJS) && $jsfile == true ){
							$fpjs = fopen($blockJS, 'w'); // Open JS file
							fwrite($fpjs, $js);
							fclose($fpjs);
						}

                        // Adding imported Block into the Block list
						$blockList[] = array('blockname' => $blockName, 'status' => __('Imported', 'acf-block-generator') );

						$status = true;
					}
					
				}
				$duplicate = false;

			} else {
                // Adding Dublicate Block into the Block list
				$blockList[] = array('blockname' => $blockName, 'status' => __('Not Imported, a Block with the same name Already Exists', 'acf-block-generator') );
				$duplicate = true;
			}
	
		}

	}
    ?>
	
    <!-- Design View for Import page -->
	<div class="wrap"><div id="icon-tools" class="icon32"></div>
		<h1><?php _e('Import Block', 'acf-block-generator'); ?></h1>

		<?php if(!isset($_REQUEST['import'])){ ?>

			<p><?php _e('Howdy! Upload your Blocks JSON file and we’ll import the Blocks into this site and create the required files in your theme.', 'acf-block-generator'); ?></p>
			<p><?php _e('Choose a WXR (.json) file to upload, then click Upload file and import.', 'acf-block-generator'); ?></p>
			
            <form action="" method="post" enctype="multipart/form-data">
				<div class="wrap">
					<label for="upload"><?php _e('Choose a file from your computer', 'acf-block-generator'); ?>:</label>
					<input type="file" name="upload" value="Upload" id="upload" />
				</div>
				<div class="wrap">
					<input class="button button-primary" type="submit" name="import" value="<?php _e('Upload file and import', 'acf-block-generator'); ?>" id="import" disabled/>
				</div>
			</form>

		    <?php 
        }

		if($duplicate == true){ ?>
			<p><?php _e('Some Block(s) are not imported. If you want to import these Blocks, please make sure that there are no Blocks with the same name on your site.', 'acf-block-generator'); ?></p>
		<?php }

		if($status == true && $duplicate == false ){ ?>
			<p><?php _e('All done.', 'acf-block-generator'); ?> <a href="<?php echo get_admin_url(); ?>/edit.php?post_type=mo-block"><?php _e('Have fun!', 'acf-block-generator'); ?></a></p>
		<?php }

		// Showing the status of the imported Blocks
		if(!empty($blockList)){ ?>
			<h2><?php _e('Block List', 'acf-block-generator'); ?></h2>
			<ul>
                <?php foreach($blockList as $keyblock){ ?>
                    <li><strong><?php echo $keyblock['blockname']; ?>:</strong> <?php echo $keyblock['status']; ?></li>
                <?php } ?>
			</ul>
		<?php } ?>
	
	</div>

	<!-- Script for the 'Upload file and import' Button -->
	<script> 
        jQuery('#upload').change(function(){
            if( document.getElementById('upload').files.length == 0 ){
                jQuery('#import').attr('disabled', 'disabled');
            } else {
                jQuery('#import').removeAttr('disabled');
            }
        });
	</script>

    <?php
}