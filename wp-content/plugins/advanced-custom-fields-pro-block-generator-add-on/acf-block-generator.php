<?php
/**
 * Plugin Name: Advanced Custom Fields PRO Block Generator Add-On
 * Plugin URI:  https://www.maatwerkonline.nl
 * Description: Create Advanced Custom Fields PRO Blocks with easy-to-use UI
 * Version:     2.1.5
 * Author:      Maatwerk Online
 * Author URI:  https://www.maatwerkonline.nl
 * License:     GPL-2.0+
 * Text Domain: acf-block-generator
 */

// Prevent loading this file directly.
ob_start();
defined( 'ABSPATH' ) || die;

if ( ! function_exists( 'mo_cpt_load' ) ) {

	if ( file_exists( __DIR__ . '/vendor' ) ) {
		require __DIR__ . '/vendor/autoload.php';
	}

	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

	define( 'MO_CPT_VER', '2.0.6' );
	define( 'MO_CPT_URL', plugin_dir_url( __FILE__ ) );

	add_action( 'init', 'mo_cpt_load', 0 );

	function mo_cpt_load() {
		load_plugin_textdomain( 'acf-block-generator' );

		new MOB\BlockRegister;

		if ( ! is_admin() ) {
			return;
		}

		// Show Meta Box admin menu.
		add_filter( 'rwmo_admin_menu', '__return_true' );

		// Retrieves the ACF version.
		function get_acf_pro_version_for_acf_blocks() {
			// ACF 5 introduces `acf_get_setting`, so this might not always be available.
			if ( function_exists( 'acf_get_setting' ) ) {
				return acf_get_setting( 'version' );
			}

			// Fall back on filter use.
			// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals -- ACF hook.
			return apply_filters( 'acf/get_info', 'version' );
		}

		// Checks if Advanced Custom Fields PRO is installed and active
		if ( !class_exists( 'acf_pro' ) || get_acf_pro_version_for_acf_blocks() > '5.8.0' ) {
			add_action( 'admin_notices', 'acf_pro_for_acf_blocks_message_plugin_not_activated' );
		}

		new MOB\Edit( 'mo-block' );
	}

	// Load the AcfCallback PHP file
	if ( file_exists( __DIR__ . '/src' ) ) {
		require __DIR__ . '/src/AcfCallback.php';
	}

	// Load the BlockImportExport PHP file
	if ( file_exists( __DIR__ . '/src' ) ) {
		require __DIR__ . '/src/BlockImportExport.php';
	}


	/**
	 * Notify that we need ACF to be installed and active.
	 */
	function acf_pro_for_acf_blocks_message_plugin_not_activated() {
		$message = sprintf(
			/* translators: %1$s resolves to Advanced Custom Fields PRO Block Generator Add-On, %2$s resolves to Advanced Custom Fields PRO */
			__( '%1$s requires %2$s PRO 5.8 or higher to be installed and activated.', 'acf-block-generator' ),
			'Advanced Custom Fields PRO Block Generator Add-On',
			'Advanced Custom Fields'
		);

		printf( '<div class="error"><p>%s</p></div>', esc_html( $message ) );
	}

	
	/**
	 * Add the TextDomain of the current theme to the PHP Code field
	 */
	function wpshout_action_example() { 
		$theme_name = wp_get_theme();
	    echo '<input type="hidden" class="act_themes" value="'.esc_html( $theme_name->get( 'TextDomain' ) ).'">'; 
	}
	add_action('admin_footer', 'wpshout_action_example'); 

	
	/**
	 * ACF Save Fields Callback to add Preview information from the Default Fields to the selected Block
	 */
	function acf_save_fields_callback( $post_id, $post, $update ) {

		global $wpdb;
		$table_name  = $wpdb->prefix.'posts';
		$blockArray = array();
		$bArray = array();
		$specifications_group_id = $post_id; 
		$specifications_fields = array();

		// Get the block name from ACF location 
		$group = get_post( $post_id );
		$location = unserialize($group->post_content);

		foreach($location['location'] as $key){
			foreach($key as $key1){
				$acfLoction = $key1['value'];
			}
		}

		$acfArray = explode( "/", $acfLoction );
		$block = $acfArray[1];

		// Get the block post object 
		$result = $wpdb->get_results( "SELECT * FROM $table_name WHERE `post_name`= '$block' AND `post_type` = 'mo-block'" );

		$blockArray = json_decode( $result[0]->post_content );
		$fields = acf_get_fields( $specifications_group_id );
		
		// Findout all ACF field default value 	
		foreach($fields as $field) {
			if($field['type'] != 'image'){
				$acfData[$field['name']] = $field['default_value'] ; 	
			}
		}

		// Create a Array to add into block post JSON 	
		$previewArray = array('example' => array(
		          'attributes' => array(
		              'mode' => 'preview',
		              'data' => $acfData
		          )
			)
		);

		foreach($blockArray as $blockey => $value) {
			$bArray[$blockey] = $value;
		}

		// Merge preview Array into block post Array 	
		$finalArray = array_merge($bArray, $previewArray);
		$finalJson = json_encode($finalArray);

		$fString = str_replace('','',"$finalJson");

		// Update the converted JSON data into block post content	
		$result = $wpdb->query($wpdb->prepare("UPDATE $table_name SET post_content='$fString' WHERE `post_name`= '$block' AND `post_type` = 'mo-block'"));

		if( $result == true ){
			return true;
		} else {
			return false;
		};

	}
	add_action( 'save_post_acf-field-group', 'acf_save_fields_callback', 10, 3 );


	/**
	 * Add the ACF preview into default JSON
	 */
	function block_post_callback( $ID, $post ) {
		global $wpdb;
		
		$table_name  = $wpdb->prefix.'posts';
		$blockArr    = array();
		$finalArr    = array();

		// Get default JSON
		$blockArr = json_decode($post->post_content);


		// Preview element Array
		$previewArr = array('example' => array(
		          'attributes' => array(
		              'mode' => 'preview'		              
		          )
			)
		);

		foreach($blockArr as $blockey => $value) {
			$bArray[$blockey] = $value;
		}

		// Merge default Array with Preview Array
		$finalArr  = array_merge($bArray, $previewArr);
		$finalJson = json_encode($finalArr);
		$fString   = str_replace('','',"$finalJson");

		$result = $wpdb->query($wpdb->prepare("UPDATE $table_name SET post_content='$fString' WHERE `ID`= $ID AND `post_type` = 'mo-block'"));

		if( $result == true ){
			return true;
		} else {
			return false;
		};

	}
	add_action( 'publish_mo-block', 'block_post_callback', 10, 2 );

}