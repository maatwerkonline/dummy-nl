<?php
/**
 * Snippet Name: WooCommerce Ajax Actions
 * Description: A snippet to Ajaxify the Actions on the Order overview page of WooCommerce
 * Version: 1.0
 * Author: Nick Heurter
 * Dependent Plugin: woocommerce/woocommerce.php
 * Required: false
 * Default Active: false
 */

// Exit if accessed directly
if (!defined('ABSPATH')) {
	exit;
}
/**
 * Capture the triggering of a order action.
 */
function ace_ajaxify_admin_order_status_update() {
	?><script type="text/javascript" charset="utf-8">
		jQuery(function($) {
			$(document.body).on('click', '.wc-action-button', function(e) {
				e.preventDefault();

				var $this = $(this),
					row = $(this).parents('tr').first();

				$this.parents('.wc_actions').block({ message: null, overlayCSS: { background: '#fff', opacity: 0.6, top: 0, right: 0, left: 'unset', width: row.width(), height: row.height() } });
				$.get($(this).attr('href') + '&type=overview', {}, function(response) {
					$this.parents('.wc_actions').children().unblock();
					$this.parents('tr').first().replaceWith(response.html);
				})
			});
		});
	</script><?php
}
add_action( 'admin_head', 'ace_ajaxify_admin_order_status_update' );


/**
 * Handle AJAX event.
 * 
 * Almost exact replica of WC_AJAX::mark_order_status()
 */
function ace_ajax_mark_order_status() {
	if ( current_user_can( 'edit_shop_orders' ) && check_admin_referer( 'woocommerce-mark-order-status' ) ) {
		$status = sanitize_text_field( $_GET['status'] );

		$order  = wc_get_order( absint( $_GET['order_id'] ) );
		if ( wc_is_order_status( 'wc-' . $status ) && $order ) {
			// Initialize payment gateways in case order has hooked status transition actions.
			wc()->payment_gateways();

			$order->update_status( $status, '', true );
			do_action( 'woocommerce_order_edit_status', $order->get_id(), $status );
		}

		ace_ajaxified_admin_order_status_update_response( $order->get_id() );
	}
}
add_action( 'wp_ajax_woocommerce_mark_order_status', 'ace_ajax_mark_order_status', 5 );


/**
 * Send proper response on order processing action.
 */
function ace_ajaxified_admin_order_status_update_response( $order_id ) {

	if ( wp_doing_ajax() && isset( $_REQUEST['type'] ) && $_REQUEST['type'] == 'overview' ) {

		ob_start();
			include_once plugin_dir_path( WC_PLUGIN_FILE ) . '/includes/admin/list-tables/class-wc-admin-list-table-orders.php';
			new WC_Admin_List_Table_Orders();

			$table = _get_list_table('WP_Posts_List_Table', array( 'screen' => 'edit-shop_order' ));
			$table->single_row( $order_id );
		$html = ob_get_clean();

		wp_send_json( array(
			'success' => true,
			'html' => $html
		) );
		die;
	}
}
