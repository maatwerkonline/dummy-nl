<?php

/**
 * Snippet Name: Come Back Tab
 * Description: Adds a setting page for assigning a tab message when a site visitor moves to a different tab 
 * Version: 1.0
 * Author: Frank van 't Hof
 * Required: false
 * Default Active: false
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Register Setting for come back Tab
 */
function mo_come_back_register_settings() {

    // Update all settings
    register_setting( 'come-back-settings', 'come-back-setting' );
}
add_action( 'admin_init', 'mo_come_back_register_settings' );


/**
 * Admin Menu for come back tab
 */
function mo_tab_page_admin_menu() {
    add_theme_page( 
        __('Tab Settings', 'mos'),
        __('Tab Settings', 'mos'),
        'manage_options',
        'come-back-settings',
        'mo_come_back_settings',
    );
}
add_action( 'admin_menu', 'mo_tab_page_admin_menu' );


/**
 * Settings for come back tab
 */
function mo_come_back_settings() { 
    ?>

    <div class="wrap">

        <h1><?php _e('Tab Settings', 'mos'); ?></h1>

        <form action="options.php" method="POST">
   
            <table class="form-table" role="presentation">
                <tbody>

                    <?php
                        settings_fields( 'come-back-settings' );
                        do_settings_sections( 'come-back-settings' );
                        $option = get_option( 'come-back-setting' );
                    ?>
                    <tr>
                        <th scope="row">
                            <label for="come-back"><?php _e('Message to show when leaving tab:', 'mos'); ?></label>
                        </th>
                        <td>
                            <input style="width:500px;" type="text" value="<?php echo $option; ?>" id="come-back" name="come-back-setting">
                        </td>
                    </tr>
                </tbody>
            </table>

            <?php submit_button(); ?>

        </form>

    </div>

    <?php
}

/**
 * Load script to show the message
 */
function mo_come_back_script(){
    $option = get_option( 'come-back-setting' );
    if(!empty($option)) :
    ?>
        <script>
            window.onload = function() {

                var pageTitle = document.title;
                var attentionMessage = '<?php echo $option; ?>';

                document.addEventListener('visibilitychange', function(e) {
                    var isPageActive = !document.hidden;

                    if(!isPageActive){
                        document.title = attentionMessage;
                    }else {
                        document.title = pageTitle;
                    }
                });
            };
        </script>
    <?php
    endif;
}
add_action('wp_footer', 'mo_come_back_script'); ?>

