<?php

/**
 * Snippet Name: Load CSS with get_template_part()
 * Description: The css will be loaded from the same folder when using get_template_part(). The file name needs to be the same.
 * Version: 1.1
 * Author: Frank van 't Hof
 * Required: false
 * Default Active: false
 */


if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

// Hook into get_template_part and check if css file exists in same folder.
function get_template_part_load_css($slug) {
    if (file_exists(get_template_directory().$slug.'.css')) {
        $style = basename($slug);
        wp_enqueue_style($style, TEMPPATH.$slug.'.css');
    }
    
}
add_action( 'get_template_part', 'get_template_part_load_css');

// Load template styles in back-end editor
function block_editor_loop_css() {
    foreach( glob( get_template_directory().'/template-parts/loops/*/*.css' ) as $file ) {
        $basename = basename($file);
        $basename = str_replace('.css', '', $basename);
        wp_enqueue_style( $basename, get_template_directory_uri().'/template-parts/loops/'.$basename.'/'.$basename.'.css');
    }
}
add_action( 'enqueue_block_editor_assets', 'block_editor_loop_css', 100);


?>