<?php

/**
 * Snippet Name: Icon manager
 * Description: Easily manage icon fonts 
 * Version: 1.0 (wip)
 * Author: Melvin Brem
 * Required: false
 * Default Active: false
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Register Setting for icon manager
 */
function mo_icon_manager_register_settings() {

    // Update all settings
    register_setting( 'icon-manager-settings', 'icon-manager-setting' );
}
add_action( 'admin_init', 'mo_icon_manager_register_settings' );


/**
 * Admin Menu for come back tab
 */
function mo_icon_manager_admin_menu() {
    add_theme_page( 
        __('Icon manager', 'mos'),
        __('Icon manager', 'mos'),
        'manage_options',
        'icon-manager-settings',
        'mo_icon_manager_settings',
    );
}
add_action( 'admin_menu', 'mo_icon_manager_admin_menu' );


/**
 * Settings for the icon manager
 */
function mo_icon_manager_settings() {

    $icons_dir = get_template_directory()."/assets/icons/";

    // Get all folders inside '/assets/icons/' as seperate icon fonts
    $icon_fonts = [];

    foreach(glob($icons_dir.'*', GLOB_ONLYDIR) as $dir) {

        $dirname = basename($dir);
        array_push($icon_fonts, $dirname);

    }
    ?>

    <div class="wrap">

        <h1><?php _e('Icon manager', 'mos'); ?></h1>

        <table class="wp-list-table widefat plugins">
            <thead>
                <tr>
                    <td id="cb" class="manage-column column-cb check-column"><label class="screen-reader-text" for="cb-select-all-1"><?php _e('Sellect all','mos') ?></label><input id="cb-select-all-1" type="checkbox"></td>
                    <th scope="col" id="name" class="manage-column column-name column-primary"><?php _e('Icon font','mos') ?></th>
                </tr>
            </thead>
            <tbody id="the-list">

            <?php
            
            if(!empty($icon_fonts)):
                
                $messages_output .= "<b>".__( 'Icon font folders found:', 'my_plugin_textdomain' )."</b></br>";
        
                foreach($icon_fonts as $icon_font_name):
                    ?>

                    
                <tr style="" class="active">

                    <th scope="row" class="check-column">
                        <input type="checkbox" name="snippets_name[]" value="/home/maatwerkon/domains/devmaatwerkonline.nl/public_html/newdummy/wp-content/plugins/maatwerk-online-snippets/snippets/allow-svg">
                    </th>

                    <td class="snippet-title column-primary">

                        <strong><?php echo $icon_font_name ?></strong>

                        <div class="row-actions visible">
                            <span class="deactivate"><a href="https://newdummy.devmaatwerkonline.nl/wp-admin/options-general.php?page=mos-plugin&amp;deactivate=%2Fhome%2Fmaatwerkon%2Fdomains%2Fdevmaatwerkonline.nl%2Fpublic_html%2Fnewdummy%2Fwp-content%2Fplugins%2Fmaatwerk-online-snippets%2Fsnippets%2Fallow-svg" class="edit" aria-label="Deactivate Allow SVG">Deactivate</a></span> 
                        </div>

                    </td>

                </tr>

                    <?php
                endforeach;
        
            endif;
            
            ?>

            </tbody>
            <tfoot>
                <tr>
                    <td id="cb" class="manage-column column-cb check-column"><label class="screen-reader-text" for="cb-select-all-1"><?php _e('Sellect all','mos') ?></label><input id="cb-select-all-1" type="checkbox"></td>
                    <th scope="col" id="name" class="manage-column column-name column-primary"><?php _e('Snippet','mos') ?></th>
                </tr>
            </tfoot>
        </table>

    </div>

    <?php
}

?>

