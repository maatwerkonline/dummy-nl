jQuery(document).ready(function($) {
	"use strict";

    $( ".upload-view-toggle-snippets" ).on( "click", function( event ) {
        event.preventDefault();
        $( "body" ).toggleClass("show-upload-view");
    });
});
