<?php
// exit if accessed directly
if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('acf_field_linearicons')) :

    class acf_field_linearicons extends acf_field {

        private $icons = false;

        public function __construct($settings) {
            $this->name = 'linearicons';
            $this->label = __('Linear Icon', 'acf-custom-icons');
            $this->category = 'content';
            $this->settings = $settings;

            $this->defaults = array(
                'allow_null' => 0,
                'show_preview' => 1,
                'live_preview' => '',
                'choices' => array()
            );

            parent::__construct();

            add_action('wp_enqueue_scripts', array($this, 'frontend_enqueue_scripts'));
            add_action('admin_enqueue_scripts', array($this, 'admin_enqueue_scripts'));
        }

        private function get_icons($format = 'list') {
            global $acf_icons_array;
            $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            
            // if($actual_link == get_site_url().'/wp-admin/edit-tags.php?taxonomy=product_cat&post_type=product'){
            //     echo 'aa';
            // }else{
            //     echo 'bb';
            // }
            // exit();


            if (!$this->icons) {
                $this->icons = $acf_icons_array['linearicons']['icons'];
            }

            return $this->icons;
        }

        public function render_field_settings($field) {
            acf_render_field_setting($field, array(
                'label' => __('Allow Null?', 'acf-custom-icons'),
                'instructions' => '',
                'type' => 'radio',
                'name' => 'allow_null',
                'choices' => array(
                    1 => __('Yes', 'acf-custom-icons'),
                    0 => __('No', 'acf-custom-icons')
                )
            ));

            acf_render_field_setting($field, array(
                'label' => __('Show Icon Preview', 'acf-custom-icons'),
                'instructions' => __('Set to \'Yes\' to include a larger icon preview on any admin pages using this field.', 'acf-custom-icons'),
                'type' => 'radio',
                'name' => 'show_preview',
                'choices' => array(
                    1 => __('Yes', 'acf-custom-icons'),
                    0 => __('No', 'acf-custom-icons')
                )
            ));
        }

        public function render_field($field) {
            if ($field['allow_null']) {
                $select_value = $field['value'];
            } else {
                $select_value = ( 'null' != $field['value'] ) ? $field['value'] : $field['default_value'];
            }

            $field['type'] = 'select';
            $field['choices'] = array();
            $field['class'] = 'bm select2-customicons customicons-edit';

            $icons = $this->get_icons('list');
            foreach ($icons as $key => $value) {
                $field['choices'][$key] = htmlentities($value);
            }
            if ($field['show_preview']) :
                ?>
                <div class="icon_preview"></div>
                <?php
            endif;

            acf_render_field($field);
        }

        public function admin_enqueue_scripts() {
            wp_register_style('acfci-' . $this->name, ACFCI_PUBLIC_PATH . 'assets/css/fonts/linearicons/linearicons.css');
            wp_enqueue_style('acfci-' . $this->name);
        }

        public function frontend_enqueue_scripts() {
            wp_register_style('acfci-' . $this->name, ACFCI_PUBLIC_PATH . 'assets/css/fonts/linearicons/linearicons.css');
            wp_enqueue_style('acfci-' . $this->name);
        
        }

        public function format_value($value, $post_id, $field) {
            if ('null' == $value) {
                return false;
            }
            if (empty($value)) {
                return $value;
            }
            if (!$this->icons) {
                $this->get_icons();
            }
            $icon = isset($this->icons[$value]) ? $this->icons[$value] : false;

            if ($icon) {

                $value = '<i class="' . $value . '" aria-hidden="true"></i>';
            }
            return $value;
        }

    }

    new acf_field_linearicons($this->settings);

endif;
