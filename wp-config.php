<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'dummy' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Us~n-5Pk2jBuma=+Ory&yl.8MV4AZxh[kNIKpX`CDR0J7!W#;v:sm9cYZ9hXY=B5' );
define( 'SECURE_AUTH_KEY',  '(86K:k^wE0]T`v>c&my$ZtUx5xUNO4,5qrT8tObVFld`{Vy@T,$^nw~fmG5ou]%-' );
define( 'LOGGED_IN_KEY',    'L&hi&-@<dt6w>90aVQTamv~aGDgXhzMS4k{y!9!rIj}Wa,EEkWThhC3Lt2 0]UoS' );
define( 'NONCE_KEY',        'r1a;VHeGS5/R?W-%mL>%Kpa7|DpF2z=3dMjc[zZ+0BFEu9`,`5:t7!zGqoRU$W2[' );
define( 'AUTH_SALT',        'I#GY[Z|BY,2@CWXYo-I;D[}6{p9:C LThx?sI4^S(eC+]tGSl39-D/)>/=h@/nT!' );
define( 'SECURE_AUTH_SALT', '*WVe|NRXN $/(O[-!/U?|f`Uh[`;##V{xXU>Bk)]7}l,wTG3Csxmt$$HV0eD0Bij' );
define( 'LOGGED_IN_SALT',   '_ms;?k{rJqpj1%X9u92E&[w2e5j.k3LBg G0M;hkd`kXtBS+NJp-99OF:J{9J223' );
define( 'NONCE_SALT',       'If*+L7V4IO!#%JlFt6dT`R>{tkHn>hqNLXI7FlEPIt724}OIqkRX}x<Te(}u`t&8' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
